package exeptions;

public class WrongWeaponException extends Exception {
    public WrongWeaponException(String errorMessage) {
        super(errorMessage);
    }
}

