package gaming;

import exeptions.WrongArmorException;
import model.Armor;
import model.Hero;
import model.Weapons;

import model.enums.ItemSlot;


;

public class Game {

    //add weapon, check if req-level and heroType is ok!
    public static void addWeapon(Hero hero, Weapons weapons) throws WrongArmorException {

        if(hero.getLevel() < weapons.getReqLevel()){
            throw new WrongArmorException("You are not on the required level");

        }
        if(!hero.getHeroType().equals(weapons.getHeroType())){
            throw new WrongArmorException("you are not allowed");

        }
        else {
            hero.getWeapon().put(ItemSlot.WEAPON, weapons);
            System.out.println("Congratz, you just bought " + weapons.getName() + " " + weapons.getWeaponType());
        }
    }


    //add armor, check if req-level and heroType is ok!
    public static void addArmor(Hero hero, Armor armor) throws Exception {
        if(hero.getArmor().containsKey(armor.getSlot())){
            throw new WrongArmorException("you already got armor on that part of your body");
        }
        if(hero.getLevel() < armor.getReqLevel()){
            throw new WrongArmorException("You are not on the required level");
        }
        if(!hero.getHeroType().equals(armor.getHeroType())){
            throw new WrongArmorException("You are not allowed");
        }
        else {
            hero.getArmor().put(armor.getSlot(), armor);
        }

    }

}
