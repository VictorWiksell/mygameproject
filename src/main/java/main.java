import gaming.Game;
import model.*;
import model.enums.ArmorTypes;
import model.enums.HeroType;
import model.enums.ItemSlot;
import model.enums.WeaponType;

import java.util.ArrayList;
import java.util.List;

import static model.enums.HeroType.*;

public class main {


    public static void main(String[] args) throws Exception {


      Hero theWarrior = new Hero("Warrior", HeroType.WARRIOR);
      Armor armor1 = new Armor("Body Plate1.0", 1, ItemSlot.BODY,new PrimaryAttributes(1,1,1), ArmorTypes.PLATE);
      Game.addArmor(theWarrior,armor1);
      System.out.println(theWarrior.displayCharcter());
      Armor armor2 = new Armor("Lord Helmet", 1, ItemSlot.HEAD,new PrimaryAttributes(3,2,2), ArmorTypes.PLATE);
      Game.addArmor(theWarrior,armor2);
      System.out.println(theWarrior.displayCharcter());
        Weapons weapon = new Weapons("Dwarf Slayer",1,20,1, WeaponType.SWORDS);
        Game.addWeapon(theWarrior,weapon);
        System.out.println(theWarrior);
      System.out.println(theWarrior.getArmor());




    }
}
