package model;

import model.enums.HeroType;
import model.enums.ItemSlot;

import java.util.HashMap;

import static model.enums.HeroType.*;

public class Hero {

     private String name;
     private int level = 1;
     private HeroType heroType;
     private PrimaryAttributes primaryAttributes;
     private TotalAttributes totalAttributes;

     HashMap<ItemSlot, Armor> armor;
     HashMap<ItemSlot, Weapons> weapon;




     public Hero(String name, HeroType heroType) {
          this.name = name;
          this.heroType = heroType;
          weapon = new HashMap<>();
          armor = new HashMap<>();
          setPrimaryAttributes(heroType);
          totalAttributes = getTotalAttributes();

     }


     public String getName() {
          return name;
     }

     public void setName(String name) {
          this.name = name;
     }

     public HashMap<ItemSlot, Armor> getArmor() {
          return armor;
     }

     public void setArmor(HashMap<ItemSlot, Armor> armor) {
          this.armor = armor;
     }

     public HashMap<ItemSlot, Weapons> getWeapon() {

          return weapon;
     }

     public void setWeapon(HashMap<ItemSlot, Weapons> weapon) {
          this.weapon = weapon;
     }

     //get totalattributes, add from primary and check for all armor attributes
     public TotalAttributes getTotalAttributes() {

          double pS = getPrimaryAttributes().getStrength();
          double pD = getPrimaryAttributes().getDexterity();
          double pI = getPrimaryAttributes().getIntelligence();
          double aS = 0.0d;
          double aD = 0.0d;
          double aI = 0.0d;

               if (getArmor().get(ItemSlot.HEAD) != null){
               aS += getArmor().get(ItemSlot.HEAD).getPrimaryAttributes().getStrength();
               aD += getArmor().get(ItemSlot.HEAD).getPrimaryAttributes().getDexterity();
               aI = getArmor().get(ItemSlot.HEAD).getPrimaryAttributes().getIntelligence();

          }
               if (getArmor().get(ItemSlot.BODY) != null){
               aS += getArmor().get(ItemSlot.BODY).getPrimaryAttributes().getStrength();
               aD += getArmor().get(ItemSlot.BODY).getPrimaryAttributes().getDexterity();
               aI += getArmor().get(ItemSlot.BODY).getPrimaryAttributes().getIntelligence();

          }
               if (getArmor().get(ItemSlot.LEGS) != null){
               aS += getArmor().get(ItemSlot.LEGS).getPrimaryAttributes().getStrength();
               aD += getArmor().get(ItemSlot.LEGS).getPrimaryAttributes().getDexterity();
               aI += getArmor().get(ItemSlot.LEGS).getPrimaryAttributes().getIntelligence();

          }
          TotalAttributes totalAttributes = new TotalAttributes(pS+aS,pD+aD,pI+aI);
          return totalAttributes;

     }

     public void setTotalAttributes(TotalAttributes totalAttributes) {

          this.totalAttributes = totalAttributes;
     }


     public int getLevel() {
          return level;
     }

     public void setLevel(int level) {
          this.level = level;
     }

     // Character types are specified with enum
     public HeroType getHeroType() {
          return heroType;
     }

     public void setHeroType(HeroType heroType) {
          this.heroType = heroType;
     }

     public PrimaryAttributes getPrimaryAttributes() {
          return primaryAttributes;
     }

     //set start primaryAttributes on each character
     public void setPrimaryAttributes(HeroType heroType) {

          if (heroType.equals(MAGE)){
               this.primaryAttributes = new PrimaryAttributes(1,1,8);

          }
          if (heroType.equals(RANGER)){
               this.primaryAttributes = new PrimaryAttributes(1,7,1);
          }
          if (heroType.equals(ROUGE)){
               this.primaryAttributes = new PrimaryAttributes(2,6,1);

          }
          if (heroType.equals(WARRIOR)){
               this.primaryAttributes = new PrimaryAttributes(5,2,1);
          }
     }

     public double getDps() {
          double dps = 0.0d;
          double wDps;
          if (getWeapon().size() == 0){
              wDps = 1.0d;
          }
          else{
               wDps = getWeapon().get(ItemSlot.WEAPON).getWeaponDps();
          }
          if(getHeroType().equals(MAGE) && getWeapon() != null) {
               double totMain = getTotalAttributes().getIntelligence() / 100;
               dps = wDps * (1 + totMain);
          } if (getHeroType().equals(RANGER) && getWeapon() != null) {
               double totMain = getTotalAttributes().getDexterity() / 100;
               dps = wDps * (1 + totMain);
          } if (getHeroType().equals(ROUGE) && getWeapon() != null) {
               double totMain = getTotalAttributes().getDexterity() / 100;
               dps = wDps * (1 + totMain);
          } if (getHeroType().equals(WARRIOR) && getWeapon() != null) {
               //double totMain = getTotalAttributes().getStrength() / 100;
               dps = wDps * (1 + (getTotalAttributes().getStrength() / 100));
          }

         return dps;
     }

     //method for level up and add the right attributes for each character
     public void levelUp(){

          if (getHeroType().equals(MAGE)){
               getPrimaryAttributes().setStrength(getPrimaryAttributes().getStrength()+1);
               getPrimaryAttributes().setDexterity(getPrimaryAttributes().getDexterity()+1);
               getPrimaryAttributes().setIntelligence(getPrimaryAttributes().getIntelligence()+5);
               setLevel(getLevel()+1);
          }
          if (getHeroType().equals(RANGER)){
               getPrimaryAttributes().setStrength(getPrimaryAttributes().getStrength()+1);
               getPrimaryAttributes().setDexterity(getPrimaryAttributes().getDexterity()+5);
               getPrimaryAttributes().setIntelligence(getPrimaryAttributes().getIntelligence()+1);
               setLevel(getLevel()+1);
          }
          if (getHeroType().equals(ROUGE)){
               getPrimaryAttributes().setStrength(getPrimaryAttributes().getStrength()+1);
               getPrimaryAttributes().setDexterity(getPrimaryAttributes().getDexterity()+4);
               getPrimaryAttributes().setIntelligence(getPrimaryAttributes().getIntelligence()+1);
               setLevel(getLevel()+1);

          }
          if (getHeroType().equals(WARRIOR)){
               getPrimaryAttributes().setStrength(getPrimaryAttributes().getStrength()+3);
               getPrimaryAttributes().setDexterity(getPrimaryAttributes().getDexterity()+2);
               getPrimaryAttributes().setIntelligence(getPrimaryAttributes().getIntelligence()+1);
               setLevel(getLevel()+1);
          }
     }

     public String displayCharcter(){
          StringBuilder display = new StringBuilder();
          display.append("\r\n Name: " + getName());
          display.append("\r\n Level: " + getLevel());
          display.append("\r\n Strength: " +getTotalAttributes().getStrength());
          display.append("\r\n Dexterity: " + getTotalAttributes().getDexterity());
          display.append("\r\n Intelligence: " + getTotalAttributes().getIntelligence());
          display.append("\r\n DPS: " + getDps());

          return display.toString();
     }




     @Override
     public String toString() {
          return "Hero{" +
                  "name='" + name + '\'' +
                  ", level=" + level +
                  ", heroType=" + heroType +
                  ", primaryAttributes= " + primaryAttributes +
                  ",Total Attributes= " + getTotalAttributes() +
                  ", armor=" + armor +
                  ", weapon=" + weapon +
                  '}';
     }
}


