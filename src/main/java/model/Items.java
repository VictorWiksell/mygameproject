package model;

import model.enums.ItemSlot;

public abstract class Items {

    private String name;
    private double reqLevel;
    private ItemSlot slot;

    public Items(String name, double reqLevel, ItemSlot slot) {
        this.name = name;
        this.reqLevel = reqLevel;
        this.slot = slot;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getReqLevel() {

        return reqLevel;
    }

    public void setReqLevel(int reqLevel) {
        this.reqLevel = reqLevel;
    }

    public ItemSlot getSlot() {
        return slot;
    }

    public void setSlot(ItemSlot slot) {
        this.slot = slot;
    }

    @Override
    public String toString() {
        return "Items{" +
                "name='" + name + '\'' +
                ", reqLevel=" + reqLevel +
                ", slot=" + slot +
                '}';
    }
}
