package model;

import model.enums.HeroType;
import model.enums.ItemSlot;
import model.enums.WeaponType;

import static model.enums.HeroType.*;
import static model.enums.HeroType.WARRIOR;
import static model.enums.WeaponType.*;

public class Weapons extends Items {

    private double damage;
    private double damageSpeed;
    private double weaponDps;
    private WeaponType weaponType;
    private HeroType heroType;


    public Weapons(String name, double reqLevel, double damage, double damageSpeed, WeaponType weaponType) {
        super(name, reqLevel, ItemSlot.WEAPON);
        this.damage = damage;
        this.damageSpeed = damageSpeed;
        this.weaponType = weaponType;
        weaponDps = damageSpeed*damage;
        setHeroType(weaponType);
    }

    public double getDamage() {
        return damage;
    }

    public void setDamage(double damage) {

        this.damage = damage;
    }

    public double getDamageSpeed() {
        return damageSpeed;
    }

    public void setDamageSpeed(WeaponType weaponType) {

        this.damageSpeed = damageSpeed;
    }

    public double getWeaponDps() {
        return weaponDps;
    }

    public HeroType getHeroType() {
        return heroType;
    }

    //set req heroType for each type of weapon
    public void setHeroType(WeaponType weaponType) {

        if (weaponType.equals(STAFFS) || weaponType.equals(WANDS)){
            this.heroType = MAGE;
        }
        if (weaponType.equals(DAGGERS) || weaponType.equals(SWORDS)){
            this.heroType = ROUGE;
        }
        if(weaponType.equals(BOWS)){
            this.heroType = RANGER;
        }
        if(weaponType.equals(AXES) || weaponType.equals(SWORDS) || weaponType.equals(HAMMERS)){
            this.heroType = WARRIOR;
        }
    }


    public WeaponType getWeaponType() {
        return weaponType;
    }

    public void setWeaponType(WeaponType weaponType) {
        this.weaponType = weaponType;
    }

    @Override
    public String toString() {
        return "Weapons " +
                "name= " +getName() +
                ", damage= " + damage +
                ", damageSpeed= " + damageSpeed +
                ", WeaponDps= " + weaponDps +
                ", weaponType=" + weaponType;
    }
}
