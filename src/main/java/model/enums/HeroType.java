package model.enums;

public enum HeroType {
    MAGE,
    RANGER,
    ROUGE,
    WARRIOR
}
