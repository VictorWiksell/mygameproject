package model.enums;

public enum ItemSlot {
    HEAD,
    BODY,
    LEGS,
    WEAPON
}
