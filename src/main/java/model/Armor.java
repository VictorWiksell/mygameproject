package model;

import model.enums.ArmorTypes;
import model.enums.HeroType;
import model.enums.ItemSlot;

import static model.enums.ArmorTypes.*;
import static model.enums.HeroType.*;
import static model.enums.HeroType.WARRIOR;


public class Armor extends Items{

    private PrimaryAttributes primaryAttributes;

    private ArmorTypes armorTypes;

    private HeroType heroType;

    public Armor(String name, double reqLevel, ItemSlot slot, PrimaryAttributes primaryAttributes, ArmorTypes armorTypes) {
        super(name, reqLevel, slot);
        this.primaryAttributes = primaryAttributes;
        this.armorTypes = armorTypes;
        setHeroType(armorTypes);
    }

    public void setPrimaryAttributes(PrimaryAttributes primaryAttributes) {
        this.primaryAttributes = primaryAttributes;
    }
    public PrimaryAttributes getPrimaryAttributes() {
        return primaryAttributes;
    }


    public ArmorTypes getArmorTypes() {
        return armorTypes;
    }

    public void setArmorTypes(ArmorTypes armorTypes) {
        this.armorTypes = armorTypes;
    }


    public HeroType getHeroType() {
        return heroType;
    }

    //set req heroType for each type of armor
    public void setHeroType(ArmorTypes armorTypes) {

        if (armorTypes.equals(CLOTH)){
            this.heroType = MAGE;
        }
        if (armorTypes.equals(LEATHER) || armorTypes.equals(MAIL)){
            this.heroType = ROUGE;
        }
        if(armorTypes.equals(LEATHER) || armorTypes.equals(MAIL)){
            this.heroType = RANGER;
        }
        if(armorTypes.equals(MAIL) || armorTypes.equals(PLATE)){
            this.heroType = WARRIOR;
        }

    }

    @Override
    public String toString() {
        return "Armor{" +
                " name= " +getName() +
                " primaryAttributes= " + primaryAttributes +
                ", armorTypes= " + armorTypes +
                ", heroType= " + heroType +
                '}';
    }
}
