package model;

public class PrimaryAttributes {

    private double Strength;
    private double Dexterity;
    private double Intelligence;

    public PrimaryAttributes(double strength, double dexterity, double intelligence) {
        Strength = strength;
        Dexterity = dexterity;
        Intelligence = intelligence;
    }

    public double getStrength() {
        return Strength;
    }

    public void setStrength(double strength) {
        Strength = strength;
    }

    public double getDexterity() {
        return Dexterity;
    }

    public void setDexterity(double dexterity) {
        Dexterity = dexterity;
    }

    public double getIntelligence() {
        return Intelligence;
    }

    public void setIntelligence(double intelligence) {
        Intelligence = intelligence;
    }

    @Override
    public String toString() {
        return "primaryAttributes{" +
                "Strength=" + Strength +
                ", Dexterity=" + Dexterity +
                ", Intelligence=" + Intelligence +
                '}';
    }
}
