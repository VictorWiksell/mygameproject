package model;

import exeptions.WrongArmorException;
import gaming.Game;
import model.enums.ArmorTypes;
import model.enums.HeroType;
import model.enums.ItemSlot;
import model.enums.WeaponType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HeroTest {


    @Test
    void test_create_mage_and_start_on_level_1_and_receive_right_baseAttributes(){
        Hero theMage = new Hero("Mage", HeroType.MAGE);
        Assertions.assertEquals(theMage.getLevel(),1);
        Assertions.assertEquals(theMage.getPrimaryAttributes().getStrength(),1);
        Assertions.assertEquals(theMage.getPrimaryAttributes().getDexterity(),1);
        Assertions.assertEquals(theMage.getPrimaryAttributes().getIntelligence(),8);

    }
    @Test
    void test_create_warrior_and_start_on_level_1_and_receive_right_baseAttributes(){
        Hero theWarrior = new Hero("Warrior", HeroType.WARRIOR);
        Assertions.assertEquals(theWarrior.getLevel(),1);
        Assertions.assertEquals(theWarrior.getPrimaryAttributes().getStrength(),5);
        Assertions.assertEquals(theWarrior.getPrimaryAttributes().getDexterity(),2);
        Assertions.assertEquals(theWarrior.getPrimaryAttributes().getIntelligence(),1);
    }

    @Test
    void test_create_ranger_and_start_on_level_1_and_receive_right_baseAttributes(){
        Hero theRanger = new Hero("Ranger", HeroType.RANGER);
        Assertions.assertEquals(theRanger.getLevel(),1);
        Assertions.assertEquals(theRanger.getPrimaryAttributes().getStrength(),1);
        Assertions.assertEquals(theRanger.getPrimaryAttributes().getDexterity(),7);
        Assertions.assertEquals(theRanger.getPrimaryAttributes().getIntelligence(),1);
    }

    @Test
    void test_create_rouge_and_start_on_level_1_and_receive_right_baseAttributes(){
        Hero theRouge = new Hero("Rouge", HeroType.ROUGE);
        Assertions.assertEquals(theRouge.getLevel(),1);
        Assertions.assertEquals(theRouge.getPrimaryAttributes().getStrength(),2);
        Assertions.assertEquals(theRouge.getPrimaryAttributes().getDexterity(),6);
        Assertions.assertEquals(theRouge.getPrimaryAttributes().getIntelligence(),1);
    }

    @Test
    void test_level_up_mage_and_add_right_attribute_points(){
        Hero theMage = new Hero("Mage", HeroType.MAGE);
        double startStrength = theMage.getPrimaryAttributes().getStrength();
        double startDexterity = theMage.getPrimaryAttributes().getDexterity();
        double startIntelligence = theMage.getPrimaryAttributes().getIntelligence();

        theMage.levelUp();
        Assertions.assertEquals(theMage.getLevel(),2);
        Assertions.assertEquals(theMage.getPrimaryAttributes().getStrength(),startStrength+1);
        Assertions.assertEquals(theMage.getPrimaryAttributes().getDexterity(),startDexterity+1);
        Assertions.assertEquals(theMage.getPrimaryAttributes().getIntelligence(),startIntelligence+5);
    }

    @Test
    void test_level_up_ranger_and_add_right_attribute_points(){
        Hero theRanger = new Hero("Ranger", HeroType.RANGER);
        double startStrength = theRanger.getPrimaryAttributes().getStrength();
        double startDexterity = theRanger.getPrimaryAttributes().getDexterity();
        double startIntelligence = theRanger.getPrimaryAttributes().getIntelligence();

        theRanger.levelUp();
        Assertions.assertEquals(theRanger.getLevel(),2);
        Assertions.assertEquals(theRanger.getPrimaryAttributes().getStrength(),startStrength+1);
        Assertions.assertEquals(theRanger.getPrimaryAttributes().getDexterity(),startDexterity+5);
        Assertions.assertEquals(theRanger.getPrimaryAttributes().getIntelligence(),startIntelligence+1);
    }

    @Test
    void test_level_up_rouge_and_add_right_attribute_points(){
        Hero theRouge = new Hero("Rouge", HeroType.ROUGE);
        double startStrength = theRouge.getPrimaryAttributes().getStrength();
        double startDexterity = theRouge.getPrimaryAttributes().getDexterity();
        double startIntelligence = theRouge.getPrimaryAttributes().getIntelligence();

        theRouge.levelUp();
        Assertions.assertEquals(theRouge.getLevel(),2);
        Assertions.assertEquals(theRouge.getPrimaryAttributes().getStrength(),startStrength+1);
        Assertions.assertEquals(theRouge.getPrimaryAttributes().getDexterity(),startDexterity+4);
        Assertions.assertEquals(theRouge.getPrimaryAttributes().getIntelligence(),startIntelligence+1);
    }

    @Test
    void test_level_up_warrior_and_add_right_attribute_points(){
        Hero theWarrior = new Hero("Warrior", HeroType.WARRIOR);
        double startStrength = theWarrior.getPrimaryAttributes().getStrength();
        double startDexterity = theWarrior.getPrimaryAttributes().getDexterity();
        double startIntelligence = theWarrior.getPrimaryAttributes().getIntelligence();

        theWarrior.levelUp();
        Assertions.assertEquals(theWarrior.getLevel(),2);
        Assertions.assertEquals(theWarrior.getPrimaryAttributes().getStrength(),startStrength+3);
        Assertions.assertEquals(theWarrior.getPrimaryAttributes().getDexterity(),startDexterity+2);
        Assertions.assertEquals(theWarrior.getPrimaryAttributes().getIntelligence(),startIntelligence+1);
    }

    @Test
    void test_primaryAttributes_is_same_as_totalAttributes_if_no_armor(){
        Hero theWarrior = new Hero("Warrior", HeroType.WARRIOR);

        Assertions.assertEquals(theWarrior.getPrimaryAttributes().getIntelligence(),theWarrior.getTotalAttributes().getIntelligence());
        Assertions.assertEquals(theWarrior.getPrimaryAttributes().getStrength(),theWarrior.getTotalAttributes().getStrength());
        Assertions.assertEquals(theWarrior.getPrimaryAttributes().getDexterity(),theWarrior.getTotalAttributes().getDexterity());
    }

    @Test
    void test_calculate_weapons_dps(){
        Weapons weapon = new Weapons("Dwarf Slayer",4,20,1, WeaponType.SWORDS);
        Assertions.assertEquals(weapon.getDamage()*weapon.getDamageSpeed(),weapon.getWeaponDps());
    }

    @Test
    void test_warrior_dps_is_estimate_weaponDps_as_1_if_no_weapon(){
        Hero theWarrior = new Hero("Warrior", HeroType.WARRIOR);
            assertEquals(theWarrior.getDps(), 1*(1+theWarrior.getTotalAttributes().getStrength()/100));
    }
    @Test
    void test_mage_dps_is_estimate_weaponDps_as_1_if_no_weapon(){
        Hero theMage = new Hero("Mage", HeroType.MAGE);
        assertEquals(theMage.getDps(), 1*(1+theMage.getTotalAttributes().getIntelligence()/100));
    }
    @Test
    void test_rouge_dps_is_estimate_weaponDps_as_1_if_no_weapon(){
        Hero theRouge = new Hero("Rouge", HeroType.ROUGE);
        assertEquals(theRouge.getDps(), 1*(1+theRouge.getTotalAttributes().getDexterity()/100));
    }
    @Test
    void test_ranger_dps_is_estimate_weaponDps_as_1_if_no_weapon(){
        Hero theRanger = new Hero("Ranger", HeroType.RANGER);
        assertEquals(theRanger.getDps(), 1*(1+theRanger.getTotalAttributes().getDexterity()/100));
    }
    @Test
    void test_primaryAttributes_is_equal_to_totalAttributes_if_armor_is_empty(){
        Hero theWarrior = new Hero("Warrior", HeroType.WARRIOR);
            Assertions.assertEquals(theWarrior.getTotalAttributes().getStrength(),theWarrior.getPrimaryAttributes().getStrength());
            Assertions.assertEquals(theWarrior.getTotalAttributes().getDexterity(),theWarrior.getPrimaryAttributes().getDexterity());
            Assertions.assertEquals(theWarrior.getTotalAttributes().getIntelligence(),theWarrior.getPrimaryAttributes().getIntelligence());
    }

    @Test
    void test_calculate_hero_dps_with_weapon() throws WrongArmorException {
        Hero theWarrior = new Hero("Warrior", HeroType.WARRIOR);
        Weapons weapon = new Weapons("Dwarf Slayer",1,7,1.1, WeaponType.SWORDS);
        Game.addWeapon(theWarrior,weapon);
        Assertions.assertEquals((7*1.1)*(1+0.05), theWarrior.getDps());
    }

    @Test
    void test_calculate_hero_dps_with_weapon_and_armor() throws Exception {
        Hero theWarrior = new Hero("Warrior", HeroType.WARRIOR);
        Weapons weapon = new Weapons("Dwarf Slayer",1,7,1.1, WeaponType.SWORDS);
        Game.addWeapon(theWarrior,weapon);
        Armor armor1 = new Armor("Body Plate1.0", 1, ItemSlot.BODY,new PrimaryAttributes(1,1,1), ArmorTypes.PLATE);
        Game.addArmor(theWarrior,armor1);
        Assertions.assertEquals((7*1.1)*(1+0.06), theWarrior.getDps());
    }

}