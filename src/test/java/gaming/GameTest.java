package gaming;


import exeptions.WrongArmorException;
import model.Armor;
import model.Hero;
import model.PrimaryAttributes;
import model.Weapons;
import model.enums.ArmorTypes;
import model.enums.HeroType;
import model.enums.ItemSlot;
import model.enums.WeaponType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.function.BooleanSupplier;

import static org.junit.jupiter.api.Assertions.*;


class GameTest {

    @Test
    void test_create_and_add_armor() throws Exception {
        Hero theWarrior = new Hero("Warrior", HeroType.WARRIOR);
        Armor armor1 = new Armor("Body Cloth", 1, ItemSlot.BODY,new PrimaryAttributes(1,1,1), ArmorTypes.PLATE);
        Game.addArmor(theWarrior,armor1);
        assertTrue(theWarrior.getArmor().containsKey(ItemSlot.BODY));
    }

    @Test
    void should_fail_add_armor_if_hero_level_is_too_low(){
        Hero theWarrior = new Hero("Warrior", HeroType.WARRIOR);
        Armor armor1 = new Armor("Body Cloth", 5, ItemSlot.BODY,new PrimaryAttributes(1,1,1), ArmorTypes.PLATE);
        try {
            Game.addArmor(theWarrior,armor1);
            fail("Failed to add armor");
        }
        catch (Exception e){
            assertNotNull(e);
        }
    }

    @Test
    void should_fail_if_hero_add_wrong_type_of_armor() {
        Hero theWarrior = new Hero("Warrior", HeroType.WARRIOR);
        Armor armor1 = new Armor("Body Cloth", 1, ItemSlot.BODY,new PrimaryAttributes(1,1,1), ArmorTypes.CLOTH);
        try {
            Game.addArmor(theWarrior,armor1);
            fail("Failed to add armor");
        }
        catch (Exception e){
            assertNotNull(e);
        }
    }

    @Test
    void should_fail_if_hero_add_armor_on_not_empty_slot() throws Exception {
        Hero theWarrior = new Hero("Warrior", HeroType.WARRIOR);
        Armor armor1 = new Armor("Body Plate1.0", 1, ItemSlot.BODY,new PrimaryAttributes(1,1,1), ArmorTypes.PLATE);
        Game.addArmor(theWarrior,armor1);
        Armor armor2 = new Armor("Body Plate2.0", 1, ItemSlot.BODY,new PrimaryAttributes(1,1,1), ArmorTypes.PLATE);
        try {
            Game.addArmor(theWarrior,armor2);
            fail("Failed to add armor");
        }
        catch (Exception e){
            assertNotNull(e);
        }
    }

    @Test
    void test_armorAttributes_will_be_added_to_total_attributes() throws Exception {
        Hero theWarrior = new Hero("Warrior", HeroType.WARRIOR);
        Armor armor1 = new Armor("Body Plate1.0", 1, ItemSlot.BODY,new PrimaryAttributes(1,1,1), ArmorTypes.PLATE);
        Game.addArmor(theWarrior,armor1);

        Assertions.assertEquals(theWarrior.getPrimaryAttributes().getIntelligence()+1,theWarrior.getTotalAttributes().getIntelligence());
        Assertions.assertEquals(theWarrior.getPrimaryAttributes().getStrength()+1,theWarrior.getTotalAttributes().getStrength());
        Assertions.assertEquals(theWarrior.getPrimaryAttributes().getDexterity()+1,theWarrior.getTotalAttributes().getDexterity());
    }

    @Test
    void test_two_different_armor_attributes_will_be_added_to_totalAttributes() throws Exception {
        Hero theWarrior = new Hero("Warrior", HeroType.WARRIOR);
        Armor armor1 = new Armor("Body Plate1.0", 1, ItemSlot.BODY,new PrimaryAttributes(1,1,1), ArmorTypes.PLATE);
        Game.addArmor(theWarrior,armor1);
        Armor armor2 = new Armor("Lord Helmet", 1, ItemSlot.HEAD,new PrimaryAttributes(3,2,2), ArmorTypes.PLATE);

        Game.addArmor(theWarrior,armor2);

        Assertions.assertEquals(theWarrior.getPrimaryAttributes().getStrength()+(1+3),theWarrior.getTotalAttributes().getStrength());
        Assertions.assertEquals(theWarrior.getPrimaryAttributes().getDexterity()+(1+2),theWarrior.getTotalAttributes().getDexterity());
        Assertions.assertEquals(theWarrior.getPrimaryAttributes().getIntelligence()+(1+2),theWarrior.getTotalAttributes().getIntelligence());
    }

    @Test
    void test_three_different_armor_attributes_will_be_added_to_totalAttributes() throws Exception {
        Hero theWarrior = new Hero("Warrior", HeroType.WARRIOR);
        Armor armor1 = new Armor("Body Plate1.0", 1, ItemSlot.BODY,new PrimaryAttributes(1,1,1), ArmorTypes.PLATE);
        Game.addArmor(theWarrior,armor1);
        Armor armor2 = new Armor("Lord Helmet", 1, ItemSlot.HEAD,new PrimaryAttributes(3,2,2), ArmorTypes.PLATE);
        Game.addArmor(theWarrior,armor2);
        Armor armor3 = new Armor("don't break a leg", 1, ItemSlot.LEGS,new PrimaryAttributes(2,2,2), ArmorTypes.PLATE);
        Game.addArmor(theWarrior,armor3);
        Assertions.assertEquals(theWarrior.getPrimaryAttributes().getStrength()+(1+3+2),theWarrior.getTotalAttributes().getStrength());
        Assertions.assertEquals(theWarrior.getPrimaryAttributes().getDexterity()+(1+2+2),theWarrior.getTotalAttributes().getDexterity());
        Assertions.assertEquals(theWarrior.getPrimaryAttributes().getIntelligence()+(1+2+2),theWarrior.getTotalAttributes().getIntelligence());
    }

    @Test
    void test_create_and_add_weapon() throws Exception {
        Hero theWarrior = new Hero("Warrior", HeroType.WARRIOR);
        Weapons weapon = new Weapons("Dwarf Slayer",1,20,1, WeaponType.SWORDS);;
        Game.addWeapon(theWarrior,weapon);
        assertTrue(theWarrior.getWeapon().containsKey(ItemSlot.WEAPON));
    }

    @Test
    void test_should_fail_if_req_level_dont_match()  {
        Hero theWarrior = new Hero("Warrior", HeroType.WARRIOR);
        Weapons weapon = new Weapons("Dwarf Slayer",4,20,1, WeaponType.SWORDS);;
        try {
            Game.addWeapon(theWarrior,weapon);
            fail("Failed to add weapon");
        }
        catch (Exception e){
            assertNotNull(e);
        }
    }

    @Test
    void test_should_fail_if_weaponType_dont_match()  {
        Hero theWarrior = new Hero("Warrior", HeroType.WARRIOR);
        Weapons weapon = new Weapons("Knife of Mora",1,20,1, WeaponType.DAGGERS);;
        try {
            Game.addWeapon(theWarrior,weapon);
            fail("Failed to add weapon");
        }
        catch (Exception e){
            assertNotNull(e);
        }
    }

    @Test
    void test_new_weapon_will_replace_the_old_one() throws WrongArmorException {
        Hero theWarrior = new Hero("Warrior", HeroType.WARRIOR);
        Weapons weapon = new Weapons("Dwarf Slayer",1,20,1, WeaponType.SWORDS);
        Game.addWeapon(theWarrior,weapon);
        Weapons weapon2 = new Weapons("The crooked sable",1,25,1, WeaponType.SWORDS);
        Game.addWeapon(theWarrior,weapon2);

        Assertions.assertEquals(theWarrior.getWeapon().size(),1);
        Assertions.assertEquals(theWarrior.getWeapon().get(ItemSlot.WEAPON).getName(),weapon2.getName());
        Assertions.assertEquals(theWarrior.getWeapon().get(ItemSlot.WEAPON).getDamage(),weapon2.getDamage());
    }


}